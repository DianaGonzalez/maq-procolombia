$(document).ready(function() {
    // nice select 
    $('select').niceSelect();

    // hover main menu
    $('nav .cont_left ul li').hover(
        function() {
            $(this).addClass('view');
            let liSelect = $(this).text();
            switch (liSelect) {
                case 'EXPORTACIONES':
                    $(this).addClass('expor');
                    $('.nav-option nav.expor').addClass('view').siblings('nav').removeClass('view')
                    break;
                case 'INVERSIÓN':
                    $(this).addClass('inver');
                    $('.nav-option nav.inver').addClass('view').siblings('nav').removeClass('view')
                    break
                case 'TURISMO':
                    $(this).addClass('turis');
                    $('.nav-option nav.turis').addClass('view').siblings('nav').removeClass('view')
                    break
                case 'MARCA PAÍS':
                    $(this).addClass('marca');
                    $('.nav-option nav.marca').addClass('view').siblings('nav').removeClass('view')
                    break

                default:
                    break;
            }
            if ($(this).hasClass('view')) {
                $(this).siblings().removeClass();
            }
        },
    );

    // hover option menu
    $('.nav-option nav').hover(
        function() {
            $(this).addClass('view');
            if ($(this).hasClass('view')) {
                $(this).siblings().removeClass('view');
            }
        },
        function() {
            $(this).removeClass('view');
            $('nav .cont_left ul li').removeClass();
        }
    );

    // mobile
    $('.mobile-button').click(
        function() {
            $(this).toggleClass('close');
            $('.mobile__content').toggleClass('open');
            $('body').toggleClass('cap');
        }
    )

    $('.mobile__menu li').click(
        function() {
            var content = $(this).children(".mobile__option");
            if (content.css("display") == "none") {
                $(this).addClass('open');
                $(this).siblings().removeClass();
                $(".mobile__option").slideUp(250);
                content.slideDown(250);
                $(this).addClass("open");
            } else {
                content.slideUp(250);
                $(this).removeClass("open");
            }
        }
    )

    // hamburger menu 
    $('.header-mobile__hamburger div').click(
        function() {
            $(this).toggleClass('open');
            $('.nav-opt-mobile').toggleClass('open');
        }
    )

    // hamburger menu nosotros
    $('.nav-opt-mobile__menu .nav-opt-content').click(
        function() {
            $(this).toggleClass('open');
            $('.nav-opt-mobile__nost').toggleClass('open');
        }
    )

    // hamburger menu nosotros more items
    $('.nav-opt-mobile__nost .nav-opt-items').click(
        function() {
            $(this).toggleClass('open');
            $(this).parent().toggleClass('active')
            $('.nav-opt-mobile__items').toggleClass('open');
        }
    )

    // Open the modal login 
    $('.second-menu__login').click(
        function() {
            $('.second-menu__modal').toggleClass('active');
        }
    )

    // Close the modal login 
    $('.second-menu__modal .close').click(
        function() {
            $('.second-menu__modal').removeClass('active');
        }
    )
});